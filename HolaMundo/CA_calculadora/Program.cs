﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_calculadora
{ 
    class Program
    {
        static void Main(string[] args)
        {

            #region METODOS ESTATICOS cubiertos 1234

            int valor = Calculadora.Sumar(5, 4);
            Console.WriteLine(valor);
          
            #endregion  


            if (Calculadora.Espar(10))
                Console.WriteLine("es par");
            else
                Console.WriteLine("es impar");

            if (Calculadora.Espar(5))
                Console.WriteLine( " es par");
            else
                Console.WriteLine("es impar");
            Console.ReadLine();
        }
    }
}
