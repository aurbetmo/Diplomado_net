﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_calculadora
{
    class Calculadora
    {

        public static int Sumar(int operador1, int operador2)
        {
            return operador1 + operador2;
        }

        public static bool Espar(int numero)
        {
            return numero % 2 == 0;
                
        }
    }
}
