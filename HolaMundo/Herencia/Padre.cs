﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    public class Padre
    {
        public int  propiedad1Padre { get; set; }
        public int propiedad2Padre { get; set; }

        public void  MetodoPadre()
        {

            Console.WriteLine( "Metodo del padre" );

        }

    }
}
