﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    public  class Hija:Padre
    {
        public int PropiedadHija { get; set; }

        public void MetodoHija()
        {
            Console.WriteLine("Metodo clase hija");


        }
    }
}
