﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConociendoCSharp
{
    public class Persona
    {
        public string NombreCompleto { get; set; }

        public int Edad
        {
            get {

                int edad = DateTime.Today.AddTicks(-FechaNacimiento.Ticks).Year - 1;

                return edad;
                }
        }

        public DateTime FechaNacimiento { get; set; }

        public bool? Casado { get; set; }

        public Genero GeneroPersona { get; set; }

        
        public Persona()
        {
            //ctor: sniper constructor
        }

        public Persona(string nombre, int edad)
        {
            NombreCompleto = nombre;
            //Edad = edad;
            FechaNacimiento = DateTime.Now;
        }

       
        public Persona(string nombre, int edad, int ano, int mes, int dia)
        {
            NombreCompleto = nombre;
            //Edad = edad;
            FechaNacimiento = new DateTime(ano, mes, dia);
        }

        public void MostraInformacion()
        {
            Console.WriteLine(" Nombre: {0}, Edad:{1}", NombreCompleto,Edad );

        }

        public void MostrarTodainformacion()
        {
                        
            Console.WriteLine(" EJERCICIO {0}, tiene {1} años y {2} ", NombreCompleto, Edad, Casado.Value ? " está casado": " no está casado");
            
        }

        public void MayorEdad()
        {

            Console.WriteLine("  {0},  {1} ", NombreCompleto, Edad > 17 ? " es mayor de edad " : " es menor de edad");
        }

        public bool EsMayorEdad()
        {

            return Edad > 17 ;
        }
    }
}
