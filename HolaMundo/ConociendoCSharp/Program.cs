﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConociendoCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            #region [Instanciando persona1 con constructor sin parametros]
            Persona persona1 = new Persona();
            persona1.NombreCompleto = "Juan";
           // persona1.Edad = 37;
            persona1.FechaNacimiento = new DateTime(1968, 8, 12);
            persona1.GeneroPersona = Genero.Masculino;
            persona1.Casado = true;
            
            #endregion

            #region [instanciando sin constructor y con propiedad entre llaves ]
            Persona persona2 = new Persona
            {
                NombreCompleto = "pepe",
                //Edad = 12,
                FechaNacimiento = new DateTime(1985, 5, 9),
                Casado = true,
                GeneroPersona = Genero.Femenino
               
            };
            #endregion

            #region Imprmir valores de persona
            Console.WriteLine("Nombre: " + persona1.NombreCompleto + " edad: " + persona1.Edad + " fecha nacimiento: " + persona1.FechaNacimiento);
            Console.WriteLine("Nombre: {0} edad: {1} fecha de nacimiento {2}", persona2.NombreCompleto, persona2.Edad, persona2.FechaNacimiento); 
            #endregion

            //Para saltar de linea
            Console.WriteLine("{0}{0}{0}",Environment.NewLine );

            #region Instanciando con el constructor de dos parametros
            Persona persona3 = new Persona("Mariana", 7);
            persona3.GeneroPersona = Genero.Femenino;
            persona3.Casado = false;
            persona3.FechaNacimiento = new DateTime(1975, 5, 9);
            Console.WriteLine($"Nombre: { persona3.NombreCompleto}, Edad :{persona3.Edad}, Fecha : {persona3.FechaNacimiento}");

            #endregion
            Console.WriteLine("{0}{0}{0}", Environment.NewLine);


            #region Instanciando constructor con 5 parametros
            Persona persona4 = new Persona("Aureliano", 15, 1979, 10, 14);
            persona4.GeneroPersona = Genero.Masculino;
            persona4.Casado = true;
            string informacion = string.Format("cinco parametros: Nombre: {0}, edad:{1}, Fecha Nacimiento:{2}", persona4.NombreCompleto, persona4.Edad, persona4.FechaNacimiento.ToString("yyyy-MM-dd"));
                        
            Console.WriteLine( informacion);
            #endregion
            Console.WriteLine("{0}{0}{0}", Environment.NewLine);

            #region Valores nulleables
            //hasvalue pregunta si tiene valor.
            int? valor = null;
            if (!valor.HasValue)
            {
                valor = 10;
            }

            #endregion

            #region Invocacion de metodos
            persona1.MostraInformacion();
            persona1.MostrarTodainformacion();
            persona2.MostrarTodainformacion();
            persona3.MostrarTodainformacion();
            persona4.MostrarTodainformacion();


         
            Console.WriteLine("{0}{0}{0}", Environment.NewLine);

            persona1.MayorEdad();
            persona4.MayorEdad();

            Console.WriteLine("{0}{0}{0}", Environment.NewLine);

            if (persona4.EsMayorEdad())
                Console.WriteLine( persona4.NombreCompleto + " es mayor edad");
            else
                Console.WriteLine(persona4.NombreCompleto + " es menor edad");
            #endregion

            #region manejo de generics

            List<int> numerosEnteros = new List<int>();

            numerosEnteros.Add(1);
            numerosEnteros.Add(2);


            foreach (int item in numerosEnteros)
            {
                Console.WriteLine(item);
            }

            List<string> listadosString = new List<string>();

            listadosString.Add("Aureliano");
            listadosString.Add("Adiela");
            listadosString.Add("Tomas");

            //otra manera

            List<string> otrolistado = new List<string>()
            {

                "Pepito",
                "juanita",
                "catalina"
            };

            foreach (string item in listadosString)
            {
                Console.WriteLine(item);
            }

            List<Persona> listapersonas = new List<Persona>();

           // listapersonas.Add(persona1);
            listapersonas.Add(persona2);
            //listapersonas.Add(persona3);
            //listapersonas.Add(persona4);
            Console.WriteLine("muestra toda informacion");

          //  listapersonas.Add(new Persona { NombreCompleto = "Raquel",  Casado = true });
            foreach (Persona item in listapersonas)
            {

                item.MostrarTodainformacion();
                //Console.WriteLine("otro dato {0}", item.NombreCompleto);
            }

            #endregion

            Console.ReadLine();

        }
    }
}
