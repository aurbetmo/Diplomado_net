﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace manejoExcepciones
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {

                //int i = 0;
                //int j = 10/i;

                string dato= null;
                funcion(dato);
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                
            }

            Console.ReadLine();
        }

        static void funcion(string s)
        {
            if (s== null)
            {
                throw new Exception("El valor está nulo");
            }

        }
    }
}
