﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionStringsMath
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Conversion de tipos
            string strnumero1 = "1213";

            //int numero1 = int.Parse(strnumero1);
            int numero1;
            bool esnumero1 = int.TryParse(strnumero1, out numero1);
            //out significa por referencia. 
            //Console.WriteLine(esnumero1 ? " es nuemero " + numero1 : "No es numero");
            //Console.ReadLine();

            //Double.TryParse
            //bool.TryParse
            // uint no recibe signos negativos.

            //Esto lo combino en una aplicacion de expresiones regulares , que aseguren que no ingresen caracteres a uncampo numerico.
            int numero2 = Convert.ToInt32(strnumero1);
            uint numero3 = Convert.ToUInt32(strnumero1);



            //Convertir a estring
            string convertirCadena = numero1.ToString();

            #endregion

            #region Manejo de Cadenas.

            string nombre = "Frase de Prueba";

            //longitud cadena

            int longitud = nombre.Length;

            //contenido dentro de una cadena, si existe
            bool siContiene = nombre.Contains("se");

            // saber la posición del primer caracter que coinncida con la busqueda.
            int indice = nombre.IndexOf("a");

            // saber la posición del ultimo caracter que coinncida con la busqueda.
            int UltimoIndice = nombre.LastIndexOf("a");

            //Cortar parte de una cadena
            string corte = nombre.Remove(5);

            //reemplazar un caracter por otro
            string nueva = nombre.Replace("a", "*");

            //Quitar espacios
            //String sinespacios = nombre.Replace(" ", "");

            //Obtener un pedazo de una cadena
            string trozo = nombre.Substring(6, 2);

            #endregion

            #region Funciones Matematicas basicas

            //Redondeo hacia arriba

            double resultadoArriba = Math.Ceiling(5.4);

            //redondeo hacia abajo
            double resultadoAbajo = Math.Floor(5.4);

            //Potencia
            double potencia = Math.Pow(2, 3);

            //raiz cuadrad
            double raizcuadrada = Math.Sqrt(9);

            //truncar
            double truncar = Math.Truncate(7.2009);

            //redondear
            double redondear = Math.Round(34.4566, 2);

            #endregion

            string ejercicio = "La controladora no encontro la ruta '/paginax' o no ha sido implmentada";
            //Obtner el nombre de esa pagina

            int indice1 = ejercicio.IndexOf("'");
            int indice2 = ejercicio.LastIndexOf("'");
            int ll_len = indice2 - (indice1 + 1) ;

            string pagina = ejercicio.Substring(indice1 + 2, ll_len-1);
            
            
            Console.WriteLine( pagina);
            Console.ReadLine();
        }
    }
}
